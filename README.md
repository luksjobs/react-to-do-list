# Aplicação de To-do-List feito em React Native com o emulador do ExpoGO

Para rodar a aplicação, por favor rode os comandos logo abaixo:

```
npm install 
#Comando para baixar as dependências do Projeto;
```

Agora que você já baixou as dependências, é só iniciar o projeto com o comando abaixo:

```
npm start
```

Se tudo ocorrer bem, o app irá rodar no localhost:19000